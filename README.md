# Java Algotrader Template Strategy

## Usage

- Clone the repository into the server you want to run it on.
- Run the script `./download_data.sh` to download historical data for
  backtesting and verification.
- Once everything seems to be working, the first run should result in:

```
No trades took place!
```

- Once you have confirmation of the same, delete the old version control system
  or reset the git remote to the respective strategy.

    - Change origin uRL (recommended)

```
git remote set-url origin <url of new strategy>
```
    - Delete the `.git` folder

```
rm -rf ./.git/  # this will remove the original version controling
```

## Modification &amp; Adaption

- You can change the `checkEntry` to modify the entry criterion
- You can modify the `Signal.java` for more complex, multi-timeout checks (_add
  an `enum` for `State` and modify said State aftet each check. Set separate
  timeouts for each state transition_)
- You can keep the existing code the same, while adding in the respective body
  of `checkSignal(int)`.

## Strategy Gotchas

- Make sure you replace the name of the strategy to match the one you're wanting
  to run.
- Make sure you verify `No trades!` before adding in strategy specific code.

## After Backtesting (Live/Paper Trading)

- In order to go live, all you need to do is override the `onTick()` function
  and shift `checkEntry()` and `checkExit()` to said function.

```
class FXDELTAService {

    // some code here
    TickVO currentTick;

    @Override
    public void onTick(TickVO tick) {
        this.currentTick = tick;
        checkEntry();
        checkExit();
    }

    private void checkEntry() {
        // change entry ruleset to check `this.currentTick` instead of
        `this.currentBar` (if required)
    }


    private void checkExit() {
        // change exit ruleset to check `this.currentTick` instead of
        `this.currentBar against this.currentPosition.sl/tp` (if required)
    }

    // some code here
}
```

- Additionally, you can switch to the `live` branch for a live template.
    - Mind you, this **cannot** be done if you delete the `.git` folder.
    - Either start your development there, or merge `live` into `master` to add
      the additional functionality.

- You will need to change `checkEntry()` and `checkExit()` to work with ticks.
