package algotrader.fxdelta;

/**
 * @brief Signal class to generate entry prices, sls and tps
 */
class Signal {
	private double pipsize = 0.0001;
	private int timeout;
	public boolean isValid = true;
	public double R;

	private enum Position {
		LONG, SHORT
	};

	private Position position;
	// stopLoss, takeProfit *MUST* be in pips
	public double stopLoss, takeProfit;
	public double entry, sl, tp;

	public String getPosition() {
		return this.position.name();
	}

	public void tradePlaced() {
		this.isValid = false;
	}

	public void updateTimeout() {
		this.timeout--;
		this.isValid = (this.timeout == 0) ? false : this.isValid;
	}

	/**
	 * @param entry:
	 *            double -> entry price
	 * @param timeout:
	 *            int -> timeout in candles
	 * @param pipsize:
	 *            double -> either 0.0001/0.01
	 * @param sl:
	 *            double -> stoploss price
	 */
	Signal(double entry, int timeout, double pipsize, double R, double sl) {
		this.entry = entry;
		this.timeout = timeout;
		this.pipsize = pipsize;
		this.R = R;

		this.sl = sl;
		this.position = (this.entry > this.sl) ? Position.LONG : Position.SHORT;
		this.stopLoss = this.sl / this.pipsize;

		this.takeProfit = this.stopLoss * this.R;
		this.tp = this.entry + ((this.position == Position.LONG) ? 1 : -1) * this.takeProfit * this.pipsize;
	}

	/**
	 * @param entry:
	 *            double -> entry price
	 * @param timeout:
	 *            int -> timeout (no of bars)
	 * @param pipsize:
	 *            double -> either 0.0001/0.01
	 * @param R:
	 *            double -> risk-to-reward
	 * @param stopLoss:
	 *            double -> stoploss (in pips)
	 * @param position:
	 *            String -> EITHER "LONG" or "SHORT"
	 */
	Signal(double entry, int timeout, double pipsize, double R, double stopLoss, String position) {
		this.entry = entry;
		this.timeout = timeout;
		this.pipsize = pipsize;
		this.R = R;

		this.stopLoss = stopLoss;
		this.position = (position.equals("LONG")) ? Position.LONG : Position.SHORT;
		this.sl = this.stopLoss * this.pipsize;

		this.takeProfit = this.stopLoss * this.R;
		this.tp = this.entry + ((this.position == Position.LONG) ? 1 : -1) * this.takeProfit * this.pipsize;
	}

}